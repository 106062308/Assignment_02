var playState = { preload: preload, create: create, update: update,  };

function preload() {
    console.log("3");
    game.load.image('pixel', 'assets/pixel.png');
}

var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];
var music;
var gunsound;
var unbeatable=false;
var shield_t=false;
var ult_num=1;
var shield_num =2;
var ult_text;
var shield_text;
function create() {
    score=0;
    ult_num = 1;
    shield_num =2;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

    music = game.add.audio('boden');
    music.play();

    gunsound = game.add.audio('gunsound');
    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(40, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    player = game.add.sprite(400, 500, 'ship');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.collideWorldBounds = true;
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    createAliens();

    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

    //  Lives
    lives = game.add.group();
    game.add.text(10, 50, 'Lives : ', { font: '34px Arial', fill: '#fff' });

    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '70px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;
    // ult
    ult_text = game.add.text(game.world.width - 100, 10, 'ult : '+ult_num, { font: '34px Arial', fill: '#fff' });
    shield_text = game.add.text(game.world.width - 155, 50, 'shield : '+shield_num, { font: '34px Arial', fill: '#fff' });
    
    for (var i = 0; i < 3; i++) 
    {
        var ship = lives.create(150 + (57 * i), 70, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.4;
    }

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    if(game.input.keyboard.justPressed(Phaser.Keyboard.U)) {
        if(music.volume<=1){
            music.volume += 0.1;
            gunsound.volume +=0.1;
        }
        if(music.volume>1) {
            music.volume=1;
            gunsound.volume =1;
        }
   
    }else if(game.input.keyboard.justPressed(Phaser.Keyboard.D)){
        if(music.volume>0){
            music.volume -= 0.1;
            gunsound.volume -=0.1;
        }
        if(music.volume<0){
            music.volume=0;
            gunsound.volume =0;
        } 
        //console.log('down to:',music.volume);
    }
}

function createAliens () {

    for (var y = 0; y < 4; y++)
    {
        for (var x = 0; x < 8; x++)
        {
            var alien = aliens.create(x * 64, y * 64, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 5, true);
            alien.play('fly');
            alien.body.moves = false;
        }
    }

    aliens.x = -20;
    aliens.y = 50;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {
    
    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown) player.body.velocity.x = -200;
        else if (cursors.right.isDown) player.body.velocity.x = +200; 
   
        if (cursors.up.isDown) player.body.velocity.y = -200; 
        else if(cursors.down.isDown)player.body.velocity.y = +200;

        if(game.input.keyboard.justPressed(Phaser.Keyboard.Z)) {
            ult_num = ult_num-1;
            ult_text.text = 'ult : '+ult_num;
            ult();
        }

        if(game.input.keyboard.justPressed(Phaser.Keyboard.X)) {
            if(shield_t===false && shield_num>0){
                shield_num=shield_num-1;
                shield_text.text =  'shield : '+shield_num;
                shield_t=true;
                player.loadTexture('toyz', 0);
                player.animations.add('flyz');
                player.animations.play('flyz', 30, true);
                unbeatable=true;
                game.time.events.add(Phaser.Timer.SECOND * 2, recover, this);
            }
        }
        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
            gunsound.play();
        }
        if (game.time.now > firingTimer)
        {
            enemyFires();
        }
        
       
        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        if(!unbeatable) game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
    }
    if(game.input.keyboard.justPressed(Phaser.Keyboard.U)) {
        if(music.volume<=1){
            music.volume += 0.1;
            gunsound.volume +=0.1;
        }
        if(music.volume>1) music.volume=1;
   
    }else if(game.input.keyboard.justPressed(Phaser.Keyboard.D)){
        if(music.volume>0){
            music.volume -= 0.1;
            gunsound.volume -=0.1;
        }
        if(music.volume<0) music.volume=0;
        //console.log('down to:',music.volume);
    }
    window.onkeydown = function() {
        if (game.input.keyboard.event.keyCode == 80){
            game.paused = !game.paused;
        }
    }
}

function collisionHandler (bullet, alien) {

    
    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();
    var emitter = game.add.emitter(alien.body.x, alien.body.y, 15);
    emitter.makeParticles('pixel');
    emitter.setYSpeed(-150, 150);
    emitter.setXSpeed(-150, 150);
    emitter.setScale(2, 0, 2, 0, 800);
    emitter.gravity = 500;
    emitter.start(true,800,null,15);
    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;

    //  And create an explosion :)
    //var explosion = explosions.getFirstExists(false);
    //explosion.reset(alien.body.x, alien.body.y);
    //explosion.play('kaboom', 30, false, true);

    


    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill');
        stateText.text = " You Won, \n Click to next level";
        stateText.visible = true;

        //music.destroy();
  
        game.input.onTap.addOnce(function(){
            game.state.start('play2');
        },this);

    }

}

function enemyHitsPlayer (player,bullet) {
    
    bullet.kill();

    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        music.destroy();
        //the "click to restart" handler
        
        game.input.onTap.addOnce(restart,this);
    }

}

function enemyFires () {

    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);

    livingEnemies.length=0;

    aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        livingEnemies.push(alien);
    });


    if (enemyBullet && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(enemyBullet,player,200);
        firingTimer = game.time.now;
    }

}

function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
        }
    }

}
/*
function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}*/

function restart () {
    score = 0;
    //  A new level starts
    
    //resets the life count
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    createAliens();
    
    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;

}

function ult(){
    enemyBullets.callAll('kill');
    game.camera.flash(0xff0000, 1500);
    if(aliens.length>0) {
        score =aliens.length*20;
        scoreText.text = scoreString + score; 
    }
    music.destroy();   
    
    aliens.removeAll();
    
        stateText.text = " You Won, \n Click to next level";
        score = 0;
        stateText.visible = true;
        game.input.onTap.addOnce(function(){
            game.state.start('play2');
        },this);
}
function recover(){
    shield_t=false;
    unbeatable=false;
    player.loadTexture('ship', 0);
}