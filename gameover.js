var gameover = { preload: preload, create: create, update: update};
var list=[];
var name_=[];
function preload(){
        //game.load.spritesheet('btnstart','asset/button-start-spritesheet.png',201,215/3);
    }
function create(){
        game.stage.backgroundColor = '#3498db';
        var a = game.add.text(150,130,'Name',{ font: '45px Arial', fill: 'yellow' });
        var b = game.add.text(370,130,'Score',{ font: '45px Arial', fill: 'yellow' });
        a.anchor.set(0.5);
        b.anchor.set(0.5);
        var w=0;
        firebase.database().ref().orderByChild('score').limitToLast(7).once('value',function(snapshot){
            for(var x in snapshot.val()){
                list[w] = snapshot.val()[x].score;
                name_[w] = snapshot.val()[x].name;
                w+=1;
            }
            quick_sort(list,0,list.length -1);
            list = list.reverse();
            name_ = name_.reverse();
            var j=0;
            for(var i=0;i<5;i++){
                j+=1;
                this.board= game.add.text(150,130+j*60,name_[i],{ font: '45px Arial', fill: 'white' });
                this.score=game.add.text(370,130+j*60,list[i],{ font: '40px Arial', fill: 'white' });
                this.board.anchor.set(0.5);
                this.score.anchor.set(0.5);
        }
        })
        var stateText = game.add.text(280,500,' Press "Enter" to Home', { font: '40px Arial', fill: '#ffc0cb' });
        //var stateText = game.add.text(200,200,' fire: space \n pause: p \n volume up,down: U,D \n', { font: '40px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);

    }
function update(){
    if(game.input.keyboard.justPressed(Phaser.Keyboard.ENTER)){
        game.state.start('menu');
    }
}

    function swap(A,i,j){
        var ele = A[i];
        A[i] = A[j];
        A[j] = ele;
         
        var ww = name_[i];
        name_[i] = name_[j];
        name_[j] = ww;
     }
             
      function quick_sort(A,p,r){
          if(p < r)
          {
             var q = partition(A,p,r);
              
              quick_sort(A, p, q-1);
              quick_sort(A, q+1, r);
              
          }
          
      }
     
     function partition(A,p,r){
          
          var x = A[r];
          var i = p - 1;
         
         for(var j = p; j <= r-1; j++)
         {
             if(A[j] <= x)
             {
                 i = i+1;
                 swap(A,i,j);
             }
         }
         
         swap(A,i+1,r);
         return i+1;
           
     }