var menuState = {

    preload: function(){
        game.load.spritesheet('btnstart','assets/button-start-spritesheet.png',256,64);

    },
    create: function(){
        var background1 = game.add.tileSprite(0, 0, 600, 600, '123');
        background1.alpha = 0.7;
        game.stage.backgroundColor = '#3498db';
        //btnstart = game.add.sprite(game.width/2, game.height/2, 'btnstart');
        //btnstart.animations.add('start', [0,1,2], 10, true);
        
        var btnstart = game.add.button(game.world.centerX,game.world.centerY+100, 'btnstart', this.actionOnClick, this, 1, 0, 2);//over out down up
        btnstart.anchor.setTo(0.5, 0.5);

        var stateText = game.add.text(game.world.centerX,200,' Raiden', { font: '60px Arial', fill: '#fff' });
        //var stateText = game.add.text(200,200,' fire: space \n pause: p \n volume up,down: U,D \n', { font: '40px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);

        var stateText = game.add.text(game.world.centerX+30,320,' Press "space" can attack\n Press "p" can plause\n Press "z" can pass the game\n Press "x" can protect you for 3 sec\n Press "d" and "u" to raise/down voice' , { font: '20px Arial', fill: '#fff' });
        //var stateText = game.add.text(200,200,' fire: space \n pause: p \n volume up,down: U,D \n', { font: '40px Arial', fill: '#fff' });
        stateText.anchor.setTo(0.5, 0.5);

    },
    update: function(){

    },
    actionOnClick: function() {

        game.state.start('play');

    
    }

}