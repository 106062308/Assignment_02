var loadState = {

    preload: function () {
    // Add a 'loading...' label on the screen
    //console.log("1");
     var loadingLabel = game.add.text(game.width/2, 300,
     'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
   // game.load.image('bar', 'assets/bar.png'); 

    var progressBar = game.add.sprite(game.width/2, 340, 'bar0');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    progressBar.animations.add('loading', [0,1,2,3,4,5,6,7,8,9], 8, true);
    progressBar.play('loading');
    //console.log("2");
    // Load all game assets
    game.load.spritesheet('btnstart','assets/button-start-spritesheet.png',256,64);
    game.load.image('123', 'assets/123.jpg');
    game.load.image('bullet', 'assets/bullet0.png');
    game.load.image('enemyBullet', 'assets/bullet44.png');
    game.load.spritesheet('invader', 'assets/invader32x32x4.png', 64, 64);
    game.load.spritesheet('toyz', 'assets/toyz.png', 32, 32);
    game.load.image('ship','assets/player.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.image('starfield', 'assets/starfield.jpg');
    game.load.audio('boden', 'assets/bodenstaendig_2000_in_rock_4bit.mp3');
    game.load.audio('gunsound', 'assets/gun sound.wav');
    game.load.spritesheet('plane','assets/humstar.png',32,32);//每個獨立個體是幾pix//用小畫家框起來看
    game.load.spritesheet('enemy', 'assets/invader32x32x4.png', 32, 32);
    game.load.image('enemy_bullet0','assets/bullet44.png');
    game.load.image('enemy_bullet1','assets/bullet44.png');
    game.load.image('enemy_bullet2','assets/bullet44.png');
    game.load.image('enemy_bullet3','assets/bullet44.png');
    game.load.image('background','assets/background.jpg');
    game.load.image('bullet','assets/bullet1.png');
    game.load.image('ship','assets/player.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.audio('gunsound', 'assets/gun sound.wav');
    game.load.audio('boden', 'assets/bodenstaendig_2000_in_rock_4bit.mp3');

    // Load a new asset that we will use in the menu state
    //game.load.image('background', 'assets/background.png');

    },

    create: function() {
    // Go to the menu state
    game.state.start('menu');
    },
    update:function(){}

}; 
/*
var game = new Phaser.Game(600, 500, Phaser.AUTO, 'canvas');
game.state.add('main', loadState);
game.state.start('main');*/