# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 有兩關，第一關敵人的子彈會追蹤，而第二關條高難度，除了追蹤子彈之外，還額外加上會反彈的子彈，令人防不勝防
2. Animations : 敵人有動畫、一次全清空敵人時有動畫，護盾有動畫。
3. Particle Systems : 攻擊到敵人時會有跟上次馬力歐撞箱子時一樣的效果。
4. Sound effects : 有背景音樂，跟空白鍵射擊的音效。
5. Leaderboard : 死掉之後輸入名字會進入。Leaderboard中按enter可以回到menu。
6. UI : p可以暫停遊戲。用u/d可以升/降音量。第一關有三條命，第二關一條。有兩個技能，一個是護盾，能夠讓人無視攻擊3秒，另一個是大絕招，讓敵人消失。

# Bonus Functions Description : 
1. 反彈子彈 : 運用collideWorldBounds讓第二關部分攻擊能夠反彈。
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]