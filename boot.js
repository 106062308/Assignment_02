
var BootState ={

    preload : function(){

        game.load.spritesheet('bar0', 'assets/22.png',224,32); 

    },

    create: function() {
        // Set some game settings.
        game.stage.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        //console.log("1");
        game.renderer.renderSession.roundPixels = true;
        // Start the load state.
        game.state.start('load');
        //console.log("1");
        },
    
        update:function(){}
}
var game = new Phaser.Game(600, 600, Phaser.AUTO, 'canvas');
game.state.add('boot', BootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('play2', playState2);
game.state.add('gameover', gameover);
game.state.start('boot');